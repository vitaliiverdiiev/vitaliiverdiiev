import { ReactElement } from 'react';
import { Link } from 'react-router-dom';

export function Header(): ReactElement {
  return (
    <header className='bg-slate-50 px-4 py-2'>
      <div className='container flex justify-between'>
        <span className='text-2xl font-medium'>vitaliiverdiiev</span>
        <ul className='flex justify-between gap-4'>
          <Link to='/'>
            <li>Home</li>
          </Link>
          <Link to='/resume'>
            <li>Resume</li>
          </Link>
          <li>Contact me</li>
        </ul>
        <select>
          <option>uk</option>
          <option>en</option>
        </select>
      </div>
    </header>
  );
}
