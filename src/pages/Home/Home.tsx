import { ReactElement } from 'react';

export default function Home(): ReactElement {
  return (
    <main>
      <div className='container'>
        <span>Home page</span>
      </div>
    </main>
  );
}
