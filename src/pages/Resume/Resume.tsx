import { ReactElement } from 'react';

export default function Resume(): ReactElement {
  return (
    <main>
      <div className='container'>
        <span>Resume page</span>
      </div>
    </main>
  );
}
